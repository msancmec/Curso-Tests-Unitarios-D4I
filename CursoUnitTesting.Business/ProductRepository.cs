﻿using CursoUnitTesting.Business.Entities;
using CursoUnitTesting.Business.Interfaces;
using CursoUnitTesting.Business.SQLDataContext;
using System;
using System.Collections.Generic;

namespace CursoUnitTesting.Business
{
    public class ProductRepository : IProductRepository
    {
        private ISQLDataContext sqlDataContext;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sqlDataContext"></param>
        public ProductRepository(ISQLDataContext sqlDataContext)
        {
            this.sqlDataContext = sqlDataContext;
        }

        public virtual IList<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            var command = "SELECT * FROM [CursoUnitTesting].[dbo].[Products]";
            sqlDataContext.OpenConnection();
            var reader = sqlDataContext.ExecuteReader(command, null);
            while (reader.Read())
            {
                var product = new Product();
                product.ProductName = reader["ProductName"].ToString();
                product.CategoryID = int.Parse(reader["CategoryID"].ToString());
                product.Created = Convert.ToDateTime(reader["Created"].ToString());
                product.QuantityPerUnit = reader["QuantityPerUnit"].ToString();
                product.UnitsInStock = int.Parse(reader["UnitsInStock"].ToString());
                product.Id = int.Parse(reader["ProductId"].ToString());
                products.Add(product);
            }
            sqlDataContext.CloseConnection();
            return products;
        }
        public Product GetProductById(int id)
        {
            Product product = new Product();
            var command = "SELECT * FROM [CursoUnitTesting].[dbo].[Products] Where ProductId =" + id.ToString();
            sqlDataContext.OpenConnection();
            var reader = sqlDataContext.ExecuteReader(command, null);
            while (reader.Read())
            {
                product.ProductName = reader["ProductName"].ToString();
                product.CategoryID = int.Parse(reader["CategoryID"].ToString());
                product.Created = Convert.ToDateTime(reader["Created"].ToString());
                product.QuantityPerUnit =reader["QuantityPerUnit"].ToString();
                product.UnitPrice = decimal.Parse(reader["UnitPrice"].ToString());
                product.UnitsInStock = int.Parse(reader["UnitsInStock"].ToString());
                product.UnitsOnOrder = int.Parse(reader["UnitsOnOrder"].ToString());
                product.Active = Convert.ToBoolean(reader["Active"]);

                product.Id = int.Parse(reader["ProductId"].ToString());
            }
            sqlDataContext.CloseConnection();
            return product;
        }
        public void Remove(int id)
        {
            var command = " DELETE FROM [CursoUnitTesting].[dbo].[Products] Where ProductID =" + id.ToString();
            sqlDataContext.OpenConnection();
            var reader = sqlDataContext.ExecuteReader(command, null);
            sqlDataContext.CloseConnection();
        }
        
        public int UpdateProduct(Product product)
        {

            var command = "UPDATE [CursoUnitTesting].[dbo].[Products] SET " +
                "ProductName = '" + product.ProductName.ToString() + "'," +
                "CategoryID = '" + product.CategoryID.ToString() + "'," +
                "QuantityPerUnit = '" + product.QuantityPerUnit.ToString() + "'," +
                "UnitPrice = '" + product.UnitPrice.ToString() + "'," +
                "UnitsInStock = '" + product.UnitsInStock.ToString() + "'," +
                "UnitsOnOrder = '" + product.UnitsOnOrder.ToString() + "'," +
                "Active = '" + (Convert.ToInt32(product.Active)).ToString() +
                "'WHERE ProductId =" + product.Id.ToString();
            sqlDataContext.OpenConnection();
            int result = sqlDataContext.ExecuteNonQuery(command, null);
            sqlDataContext.CloseConnection();

            return result;
        }

    }
}
