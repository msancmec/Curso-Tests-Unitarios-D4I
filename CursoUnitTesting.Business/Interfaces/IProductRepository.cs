﻿using CursoUnitTesting.Business.Entities;
using System.Collections.Generic;

namespace CursoUnitTesting.Business.Interfaces
{
    public interface IProductRepository
    {
         IList<Product> GetProducts();
         Product GetProductById(int id);
         int UpdateProduct(Product product);
         void Remove(int id);
        
    }
}
