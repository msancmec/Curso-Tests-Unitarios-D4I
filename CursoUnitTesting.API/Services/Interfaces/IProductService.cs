﻿using CursoUnitTesting.API.Models;
using CursoUnitTesting.Business.Entities;

namespace CursoUnitTesting.API.Services.Interfaces
{
    public interface IProductService
    {
        Product GetProductById(int id);
        void UpdateProduct(Product product);
        Product MapProductRequest(ProductUpdateRequest productUpdateRequest);
        bool CheckFootballProductAvailable(Product product);
        bool AddStock();
        void Delete(int id);
    }
}
