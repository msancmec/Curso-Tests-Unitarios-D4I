﻿using CursoUnitTesting.API.Models;
using CursoUnitTesting.API.Services.Interfaces;
using CursoUnitTesting.Business.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CursoUnitTesting.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        protected IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // GET api/product
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "product1", "product2" };
        }

        /// <summary>
        /// UpdateProduct
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public IActionResult UpdateProduct([FromBody]ProductUpdateRequest productUpdateRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                Product product = _productService.MapProductRequest(productUpdateRequest);

                _productService.UpdateProduct(product);

            }
            catch (Exception)
            {
                return StatusCode(500);
            }

            return Ok();
        }

        /// <summary>
        /// CheckFootballProductAvailable
        /// </summary>
        /// <returns></returns>
        [HttpGet("checkFootballProduct")]
        public IActionResult CheckFootballProductAvailable(int id)
        {
            bool isFootballProduct = false;
            try
            {
                Product product = _productService.GetProductById(id);
                isFootballProduct = _productService.CheckFootballProductAvailable(product);
            }
            catch (Exception ex)
            {
                if (ex.InnerException is ArgumentException)
                {
                    return BadRequest();
                }
                if (ex.Message.Contains("False"))
                {
                    return Ok(false);
                }

                return StatusCode(500);
            }

            return Ok(isFootballProduct);
        }

        [HttpGet("addStock")]
        public IActionResult AddStock()
        {
            bool stockAdded = false;
            try
            {
                stockAdded = _productService.AddStock();

            }
            catch (Exception ex)
            {
                return Ok(stockAdded);
            }

            return Ok(stockAdded);
        }

        #region Ejercicio 7: Test-Driven Development
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Product product = _productService.GetProductById(id);
            if (product == null)
            {
                return NotFound();
            }

            _productService.Delete(id);
            return Ok();
        }
        #endregion
    }
}
