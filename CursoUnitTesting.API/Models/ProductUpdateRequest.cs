﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace CursoUnitTesting.API.Models
{
    public class ProductUpdateRequest
    {
        //Validate: Number between 0 and 999999
        [BindRequired]
        [Range(1, 999999)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide the product Id,")]
        public int Id { get; set; }

        //Validate: Min Lenght 4 Max lenght 30 characters
        [BindRequired]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Please provide a product name.")]
        [StringLength(40, ErrorMessage = "Please provide a valid product name.", MinimumLength = 4)]
        public string ProductName { get; set; }

        //Validate: Max quantity of 50 per unit
        [Range(2, 9)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide a QuantityPerUnit")]
        public string QuantityPerUnit { get; set; }

        //Validate: Min value 0.0, max value 9999.99
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide a customer status")]
        [Range(0.0, 9999.99)]
        public decimal UnitPrice { get; set; }

        //Validate: 1 to 999
        [RegularExpression(@"^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$", ErrorMessage = "Please provide a valid quantity of units in stock")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide the units in stock")]
        public int UnitsInStock { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide a units on order value")]
        public int UnitsOnOrder { get; set; }

        //Validate: Active should be true
        [Range(typeof(bool), "true", "true", ErrorMessage = "The field Active must be true.")]
        public bool Active { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide an active value")]
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }
        
        ////Validate: Number between 0 and 999999999
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide the category Id,")]
        [RegularExpression(@"^(0|[1-9][0-9]*)$", ErrorMessage = "Please provide a valid category Id")]
        public int CategoryId { get; set; }
    }
}

